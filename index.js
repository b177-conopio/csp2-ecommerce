const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")

const app = express();

// Connect to MongoDB database
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.de5yx.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// prompts a message for successful connection to db
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));

// Allows all resources to access the backend application
app.use(cors());
// to read json file
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);
// Defines the "product" string to be included for all product routes in the "product" route file
app.use("/products", productRoutes);
// Defines the "product" string to be included for all product routes in the "product" route file
app.use("/orders", orderRoutes);


// App listening to port 4000 - local host
// process.env.PORT - use environment variable when deployed
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});