const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Name is required"]
	},
	description: {
		type: String,
		require: [true, "Description is required"]
	},
	price : {
		type: Number,
		require: [true, "Price is required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	isFeatured: {
		type: Boolean,
		default: false
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	buyers : [{
		email: {
			type: String,
			require: [true, "email is required"]
		},
		PurchasedOn : {
			type: Date,
			default: new Date()
		}
	}]
})

module.exports = mongoose.model("Product", productSchema);