const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		require: [true, "First name is required"]
	},
	lastName: {
		type: String,
		require: [true, "Last name is required"]
	},
	email: {
		type: String,
		require: [true, "Email is required"]
	},
	password: {
		type: String,
		require: [true, "password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	purchases: [{
		name :{
			type: String,
			require: [true, "name is required"]
		},
		PurchasedOn : {
			type: Date,
			default: new Date()
		}
	}]
})


module.exports = mongoose.model("User", userSchema);