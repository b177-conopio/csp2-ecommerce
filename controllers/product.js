const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller for creating a product
module.exports.addProduct = (data) => {

	if(data.isAdmin){

		// Create a variable "newProduct" and instantiates a new "Product" object
		let newProduct = new Product({
			name: data.product.name,
			description : data.product.description,
			price : data.product.price
		})

		// Save after instantiate
		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
			return true;
			}
		})
	}
	else{
		return Promise.resolve(false);
	};

};

// Controller for getting all active products
module.exports.getActiveProducts = () => {
		return Product.find({isActive: true}, {buyers: 0, createdOn: 0}).then(result => {
			return result;
		})
}

module.exports.getFeaturedProducts = () => {
		return Product.find({isFeatured: true}).then(result => {
			return result;
		})
}

// Controller for getting all active products
module.exports.getAllProducts = () => {
		return Product.find().then(result => {
			return result;
		})
}
// Controller for getting specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Controller function for updating a product
module.exports.updateProduct = (userData, reqParams, reqBody) => {
	
	if(userData.isAdmin){	
		// Specify the fileds/properties of the document to be updated
		let updatedProduct = {
			name : userData.product.name,
			description : userData.product.description,
			price : userData.product.price,
			isActive : userData.product.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return Promise.resolve(false);
	};
};

// Controller function for archiving a product
module.exports.archiveProduct = (userDataArchive, reqParams) => {

	if(userDataArchive.isAdmin){
		let updateActiveField = {
			isActive : false
		};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

			// Product not archived
			if (error) {

				return false;
			}
			// Product archived successfully
			else {

				return true;

			}

		});
	}
	else{
		return Promise.resolve(false);
	};
};



