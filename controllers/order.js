const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const User = require("../models/User");

// Controller for getting all orders admin
module.exports.getAllOrders = (userOrders) => {
	if(userOrders.isAdmin){
		return Order.find().then(result => {
			return result;
		})
	}
	else{
		return Promise.resolve(false);
	};	
}

module.exports.getTotalOrders = (userOrders) => {
	return Order.find().then(result => {
		return result;
	})	
}

// Controller for getting all orders admin
module.exports.getMyOrders = (data) => {
	return User.find({userId: data.id}).then(result => {
		return result;
	});
};