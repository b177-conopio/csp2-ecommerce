// imports the User model
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const Order = require("../models/Order");

module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		};
	});

};

// Controller function for user registration
module.exports.registerUser = (reqBody) => {
	// Creates variable "newUser" and instantiates a new "User" object using the mongoose model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our db
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// User authentication (/login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		// User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access token
				return { access : auth.createAccessToken(result)}
			}
			// If passwords do not match
			else{
				return false;
			}
		}
	})
}

// Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// Controller function for set as admin
module.exports.setAsAdmin = (data) => {

	if(data.isAdmin){
		let nowAdmin = {
			isAdmin : true
		}

		// Save
		return User.findByIdAndUpdate(data.userId, nowAdmin).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		});
	}
	else{
		return Promise.resolve(false);
	};
}
// Controller for checkout
module.exports.checkout = async (data) => {
	if(data.isAdmin){
		return Promise.resolve("Admins not allowed to create order");
	}
	else{
		// Add the product Id in the  array of the user
		let isUserUpdated = await User.findById(data.userId).then(user => {
			// Adds the productId in the user's purchases array
			user.purchases.push({name: data.name});

			// Save the updated user information in the database
			return user.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})
		// Add the user Id in the buyers array of the product
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			//Adds the userId in the product's buyers array
			product.buyers.push({email: data.email});

			// Save the updated product information in the database
			return product.save().then((product, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		let isOrderUpdated = await Order.create().then(order => {
			//Adds the userId in the product's buyers array
			// Create a variable "newProduct" and instantiates a new "Product" object
			let newOrder = new Order({
				userId: data.userId,
				email: data.email,
				productId : data.productId,
				name: data.name, 
				price: data.price
			})

			// Save after instantiate
			return newOrder.save().then((order, error) => {
				if(error){
					return false;
				}
				else{
				return true;
				}
			})
		})

		// Condition that will check if the user and product documents have been updated 
		// User checkout is successful
		if(isUserUpdated && isProductUpdated && isOrderUpdated){
			return true;
		}
		// User checkout failure
		else{
			return false;
		}
	}
}
// Controller for getting all orders admin
module.exports.getAllOrders = (userOrders) => {
	if(userOrders.isAdmin){
		return User.find({"purchases":{ $ne:[]}}, {isAdmin: 0, password: 0}).then(result => {
			return result;
		})
	}
	else{
		return Promise.resolve(false);
	};	
}

// Controller for getting all orders admin
module.exports.getMyOrders = (data) => {
	return User.findById(data.userId, {purchases : 1, _id: 0}).then(result => {
		return result;
	});
};