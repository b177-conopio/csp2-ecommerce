const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user 
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving user details
// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


// Route for user set as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	const data = {
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})

// Route to create order
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin, 
		name: req.body.name, 
		price: req.body.price,
		email: auth.decode(req.headers.authorization).email
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all orders
router.get("/orders", auth.verify, (req, res) => {

	const userOrders = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.getAllOrders(userOrders).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all my orders
router.get("/myOrders", auth.verify, (req, res) => {

	const myOrders = auth.decode(req.headers.authorization);

	userController.getMyOrders({userId: myOrders.id}).then(resultFromController => res.send(resultFromController));
})

module.exports = router;