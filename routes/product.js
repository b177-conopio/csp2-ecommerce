const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route for creating a product
router.post("/", auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.addProduct(data).then(resultFromController => res.send(resultFromController))
})

// Route for retrieving all active products
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
})

router.get("/featured", (req, res) => {
	productController.getFeaturedProducts().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Retrieve specific product
router.get("/:productId", (req, res) => {

	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

	const userData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(userData, req.params).then(resultFromController => res.send(resultFromController));
})

// Route for archive
router.put("/:productId/archive", auth.verify, (req, res) => {

	const userDataArchive = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(userDataArchive, req.params).then(resultFromController => res.send(resultFromController));
	
});


module.exports = router;